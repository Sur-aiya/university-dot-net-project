﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemApp.Manager;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Controllers
{
    public class TeacherController : Controller
    {
        DepartmentManager aDepartmentManager = new DepartmentManager();
        TeacherManager aTeacherManager = new TeacherManager();
        //
        // GET: /Teacher/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SaveTeacher()
        {
            ViewBag.departments = GetDepartmentDropDownList();
            ViewBag.designations = GetDesignationDropDownList();
            return View();
        }
        [HttpPost]
        public ActionResult SaveTeacher(Teacher teacher)
        {
            ViewBag.departments = GetDepartmentDropDownList();
            ViewBag.designations = GetDesignationDropDownList();
            ViewBag.msg = aTeacherManager.SaveTeacher(teacher);
            return View();
        }
        private List<SelectListItem> GetDepartmentDropDownList()
        {
            var departments = aDepartmentManager.GetAllDepartment();
            List<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Value="", Text="---Select---"}
            };
            foreach (Department dept in departments)
            {
                SelectListItem item = new SelectListItem() { Value = dept.Id.ToString(), Text = dept.Name };
                items.Add(item);
            }
            return items;
        }

        private List<SelectListItem> GetDesignationDropDownList()
        {
            var designations = aDepartmentManager.GetAllDesignation();
            List<SelectListItem> items = new List<SelectListItem>()
            {
                new SelectListItem(){Value="", Text="---Select---"}
            };
            foreach (Teacher teacher in designations)
            {
                SelectListItem item = new SelectListItem() { Value = teacher.DesignationId.ToString(), Text = teacher.DesignationName };
                items.Add(item);
            }
            return items;
        }

	}
}