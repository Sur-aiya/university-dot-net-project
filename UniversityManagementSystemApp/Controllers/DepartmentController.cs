﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UniversityManagementSystemApp.Manager;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Controllers
{
    public class DepartmentController : Controller
    {
        DepartmentManager aDepartmentManager = new DepartmentManager();
       
        TeacherManager aTeacherManager = new TeacherManager();
        CourseManager aCourseManager = new CourseManager();
        //
        //
        // GET: /Department/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SaveDepartment()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveDepartment(Department department)
        {
            ViewBag.msg = aDepartmentManager.SaveDepartment(department);
            return View();
        }

        public ActionResult ViewDepartment()
        {
            ViewBag.departments = aDepartmentManager.GetAllDepartment();
            return View();
        }
        //pari na vai
        public ActionResult CourseAssaignTeacher()
        {
            ViewBag.departments = aDepartmentManager.GetAllDepartment();
            return View();
        }
        [HttpPost]
        public ActionResult CourseAssaignTeacher(Teacher teacher)
        {
            ViewBag.departments = aDepartmentManager.GetAllDepartment();
            ViewBag.assaigned = aTeacherManager.AssaignCourseTeacher(teacher);
            return View();
        }

        public JsonResult GetTeacherCourseByDepartmentId(int id)
        {
            var teacherInfo = aDepartmentManager.GetTeacherByDepartmentId(id);
            var courseInfo = aDepartmentManager.GetCourseByDepartmentId(id);
            Department department = new Department();
            department.Courses = courseInfo;
            department.Teachers = teacherInfo;
            return Json(department);
        }

        public JsonResult GetTeacherInfoById(int id)
        {
            var teacherInfo = aDepartmentManager.GetTeacherInfoById(id);
            return Json(teacherInfo);
        }
        public JsonResult GetCourseInfoById(int id)
        {
            var courseInfo = aDepartmentManager.GetCourseInfoById(id);
            return Json(courseInfo);
        }

        public ActionResult ViewCourseStatics()
        {
            ViewBag.departments = aDepartmentManager.GetAllDepartment();

            return View();
        }
        [HttpPost]
        public ActionResult ViewCourseStatics(int id)
        {
            ViewBag.departments = aDepartmentManager.GetAllDepartment();

            return View();
        }
        public JsonResult GetCoursesByDepartmentId(int id)
        {
            var courses = aCourseManager.GetAllCourses(id);

            foreach (Course course in courses)
            {

                course.TeacherName = aTeacherManager.GetAssaigned(course.Id).ToString();
            }


            return Json(courses);
        }

        public ActionResult UnassignCourses()
        {

            return View();
        }
        [HttpPost]
        public ActionResult UnassignCourses(Course course)
        {
            ViewBag.Assigned =
            ViewBag.Message = aDepartmentManager.UnassignCourses();

            return View();
        }
        public JsonResult IsCourseAssigned(int aVal)
        {


            var returnObj = aDepartmentManager.IsCourseAssigned(aVal);


            return Json(returnObj);
        }

	}
}