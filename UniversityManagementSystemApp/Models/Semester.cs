﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UniversityManagementSystemApp.Models
{
    public class Semester
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}