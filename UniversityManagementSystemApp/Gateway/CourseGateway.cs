﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Gateway
{
    public class CourseGateway:CommonGateway
    {
        public bool IsCourseExist(Course course)
        {
            Query = "SELECT * FROM Course Where Code = '" + course.Code + "' or Name =  '" + course.Name + "' ";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
           
           
            Reader = Command.ExecuteReader();
            bool hasRow = false;
            if (Reader.HasRows)
            {
                hasRow = true;
            }
            Reader.Close();
            Connection.Close();
            return hasRow;
        }

        public int SaveCourse(Course course)
        {
            Query = "INSERT INTO Course (Code,Name,Description,Credit,DepartmentId,SemesterId)VALUES('" + course.Code + "','" + course.Name + "','" + course.Description + "','" + course.Credit + "','" + course.DepartmentId + "','" + course.SemesterId + "')";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            // Command.Parameters.Clear();

            int isRowEffected = Command.ExecuteNonQuery();
            Connection.Close();
            return isRowEffected;
        }
        public List<Semester> GetAllSemester()
        {
            Query = "SELECT * FROM SEMESTER";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Semester> semesters = new List<Semester>();
            while (Reader.Read())
            {
                Semester semester = new Semester()
                {
                    Id = (int)Reader["Id"],
                    Name = Reader["Name"].ToString()
                };
                semesters.Add(semester);
            }
            Reader.Close();
            Connection.Close();
            return semesters;
        }

        public IEnumerable<Course> GetAllCourses(int id)
        {
            Query = "select c.id, c.Code, c.Name, s.Name as SemName from Course c inner join Semester s on s.Id = c.SemesterId inner join Department d on d.Id = c.DepartmentId where d.Id =@id";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();

            Command.Parameters.Clear();
            Command.Parameters.Add("id", SqlDbType.Int);
            Command.Parameters["id"].Value = id;
            Reader = Command.ExecuteReader();
            List<Course> courses = new List<Course>();
            while (Reader.Read())
            {
                Course course = new Course()
                {
                    Id = (int)Reader["Id"],
                    Name = Reader["Name"].ToString(),
                    Code = Reader["Code"].ToString(),
                    SemName = Reader["SemName"].ToString()
                };
                courses.Add(course);
            }
            Reader.Close();
            Connection.Close();
            return courses;
        }
    }
}