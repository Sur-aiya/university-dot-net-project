﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Gateway
{
    public class TeacherGateway:CommonGateway
    {
        public bool IsEmailExist(Teacher teacher)
        {
       
            Query = "SELECT * FROM Teacher Where Email = '"+teacher.Email+"'";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
          
            Reader = Command.ExecuteReader();
            bool hasRow = false;
            if (Reader.HasRows)
            {
                hasRow = true;
            }
            Reader.Close();
            Connection.Close();
            return hasRow;
        }

        public int SaveTeacher(Teacher teacher)
        {
           // Query = "INSERT INTO Teacher VALUES(@name, @email,@address,@contactNo,@CreditToBeTaken,@RemainingCredit,@DesignationId,@DepartmentId)";
            Query = "INSERT INTO Teacher(Name, Email,Address,ContactNo,CreditToBeTaken,RemainingCredit,DesignationId,DepartmentId) Values('" + teacher.Name + "','" + teacher.Email + "','" + teacher.Address + "','" + teacher.ContactNo + "','" + teacher.CreditTake + "','" + teacher.CreditTake + "','" + teacher.DesignationId + "','" + teacher.DepartmentId + "')";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.Parameters.Clear();
           
            int isRowEffected = Command.ExecuteNonQuery();
            Connection.Close();
            return isRowEffected;
        }

        public bool IsCourseAssaigned(Teacher teacher)
        {
            //Query = "SELECT * FROM CourseAssignToTeacher Where CourseId = @cId AND Assign = @assign";
            //Query = "SELECT * FROM CourseAssignToTeacher Where CourseId = @cId ";
            Query = "SELECT * FROM CourseAssignToTeacher Where CourseId = '" + teacher.CourseId + "' AND Assign = @assign";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.Parameters.Clear();
            
            Command.Parameters.Add("assign", SqlDbType.Bit);
            Command.Parameters["assign"].Value = 1;
            Reader = Command.ExecuteReader();
            bool hasRow = false;
            if (Reader.HasRows)
            {
                hasRow = true;
            }
            Reader.Close();
            Connection.Close();
            return hasRow;
        }

        public int AssaignCourseTeacher(Teacher teacher)
        {
           // Query = "INSERT INTO CourseAssignToTeacher VALUES(@deptId, @courseId,@teacherId,@assign)";
            //Query = "INSERT INTO CourseAssignToTeacher VALUES(@deptId, @courseId,@teacherId)";
            Query = "INSERT INTO CourseAssignToTeacher VALUES('" + teacher.DepartmentId + "','" + teacher.CourseId + "','" + teacher.Id + "',@assign)";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.Parameters.Clear();

            //Command.Parameters.Add("courseId", SqlDbType.Int);
            //Command.Parameters["courseId"].Value = teacher.CourseId;

            //Command.Parameters.Add("teacherId", SqlDbType.Int);
            //Command.Parameters["teacherId"].Value = teacher.Id;
            //Command.Parameters.Add("deptId", SqlDbType.Int);
            //Command.Parameters["deptId"].Value = teacher.DepartmentId;

            Command.Parameters.Add("assign", SqlDbType.Bit);
            Command.Parameters["assign"].Value = 1;
            int isRowEffected = Command.ExecuteNonQuery();
            Connection.Close();
            return isRowEffected;
        }

        public void UpdateTeacherCredit(double remain, int id)
        {
            Query = "UPDATE Teacher SET RemainingCredit =@remain WHERE Id = @id";
            Command = new SqlCommand(Query, Connection);
            Connection.Open();
            Command.Parameters.Clear();

            Command.Parameters.Add("remain", SqlDbType.Decimal);
            Command.Parameters["remain"].Value = remain;

            Command.Parameters.Add("id", SqlDbType.Int);
            Command.Parameters["id"].Value = id;
            int isRowEffected = Command.ExecuteNonQuery();
            Connection.Close();
        }

        public string GetAssaigned(int id)
        {
            Query = "select t.Name as teacherName from Course c inner join CourseAssignToTeacher ca on c.Id = ca.CourseId inner join Teacher t on t.Id = ca.TeacherId where c.Id =@id";

            Command = new SqlCommand(Query, Connection);
            Connection.Open();

            Command.Parameters.Clear();
            Command.Parameters.Add("id", SqlDbType.Int);
            Command.Parameters["id"].Value = id;
            Reader = Command.ExecuteReader();
            string assaigned = "Not Assigned Yet";
            while (Reader.Read())
            {

                assaigned = Reader["teacherName"].ToString();

            }
            Reader.Close();
            Connection.Close();
            return assaigned;
        }
    }
}