﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemApp.Gateway;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Manager
{
    public class DepartmentManager
    {
        DepartmentGateway aDepartmentGateway = new DepartmentGateway();
        public string SaveDepartment(Department department)
        {
            
           
                //int isRowAffected = aDepartmentGateway.Save(department);
                //if (isRowAffected > 0)
                //{
                //    return "Department saved successfully";
                //}
                //else
                //{
                //    return "Department save failed";

                //}

            if (aDepartmentGateway.IsDepartmentExist(department))
            {
                return "Department already exists";
            }
            else
            {
                int isRowAffected = aDepartmentGateway.Save(department);
                if (isRowAffected > 0)
                {
                    return "Department saved successfully";
                }
                else
                {
                    return "Department save failed";

                }
            }
        }

        public List<Department> GetAllDepartment()
        {
            return aDepartmentGateway.GetAllDepartment();
        }
        public IEnumerable<Teacher> GetAllDesignation()
        {
            return aDepartmentGateway.GetAllDesignation();
        }

        public List<Teacher> GetTeacherByDepartmentId(int id)
        {
            return aDepartmentGateway.GetTeacherByDepartmentId(id);
        }

        public List<Course> GetCourseByDepartmentId(int id)
        {
            return aDepartmentGateway.GetCourseByDepartmentId(id);

        }

        public Teacher GetTeacherInfoById(int id)
        {
            return aDepartmentGateway.GetTeacherInfoById(id);
        }

        public Course GetCourseInfoById(int id)
        {
            return aDepartmentGateway.GetCourseInfoById(id);
        }

        public string UnassignCourses()
        {
            if (aDepartmentGateway.IsCoursesAssigned())
            {
                return "Course  unassigned";

            }
            else
            {
                int rowAffected = aDepartmentGateway.UnassignCourses();
                if (rowAffected > 0)
                {
                    return "Course  unassigned";
                }
                else
                {
                    return "Course unassigne falied";
                }
            }
        }

        public int IsCourseAssigned(int aVal)
        {

            if (aDepartmentGateway.IsCourseAssigned(aVal))
            {
                return 1;
            }
            return 0;
        }

    }
}