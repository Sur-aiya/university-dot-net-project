﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemApp.Gateway;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Manager
{
    public class CourseManager
    {
        CourseGateway aCourseGateway = new CourseGateway();
        public List<Semester> GetAllSemester()
        {
            return aCourseGateway.GetAllSemester();
        }

        public string Save(Course course)
        {

            //int isRowAffected = aCourseGateway.SaveCourse(course);
            //if (isRowAffected > 0)
            //{
            //    return "Course saved successfully";
            //}
            //else
            //{
            //    return "Course save failed";

            //}

            if (aCourseGateway.IsCourseExist(course))
            {
                return "Course already exists";
            }
            else
            {
                int isRowAffected = aCourseGateway.SaveCourse(course);
                if (isRowAffected > 0)
                {
                    return "Course saved successfully";
                }
                else
                {
                    return "Course save failed";

                }
            }
        }

        public IEnumerable<Course> GetAllCourses(int id)
        {
            return aCourseGateway.GetAllCourses(id);
        }
    
    }
}