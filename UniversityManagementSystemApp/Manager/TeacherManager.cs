﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UniversityManagementSystemApp.Gateway;
using UniversityManagementSystemApp.Models;

namespace UniversityManagementSystemApp.Manager
{
    public class TeacherManager
    {
       
        TeacherGateway aTeacherGateway = new TeacherGateway();
        public string SaveTeacher(Teacher teacher)
        {
            
                //int isRowAffected = aTeacherGateway.SaveTeacher(teacher);
                //if (isRowAffected > 0)
                //{
                //    return "Teacher saved successfully";
                //}
                //else
                //{
                //    return "Teacher save failed";

                //}


            if (aTeacherGateway.IsEmailExist(teacher))
            {
                return "Email already exists";
            }
            else
            {
                int isRowAffected = aTeacherGateway.SaveTeacher(teacher);
                if (isRowAffected > 0)
                {
                    return "Teacher saved successfully";
                }
                else
                {
                    return "Teacher save failed";

                }
            };
        }

        public int AssaignCourseTeacher(Teacher teacher)
        {
            int assaigned = 0;

            if (aTeacherGateway.IsCourseAssaigned(teacher))
            {
                assaigned = 2;
            }
            else
            {
                int isRowAffected = aTeacherGateway.AssaignCourseTeacher(teacher);
                if (isRowAffected > 0)
                {
                    double remain = teacher.CreditLeft - teacher.Credit;
                    aTeacherGateway.UpdateTeacherCredit(remain, teacher.Id);
                    assaigned = 1;
                }
                else
                {
                    assaigned = 2;

                }
            }

            return assaigned;
        }

        public string GetAssaigned(int id)
        {
            return aTeacherGateway.GetAssaigned(id);
        }
    }
}